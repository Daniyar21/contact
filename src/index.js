import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import reducer from "./store/reducer";

const store = createStore(reducer, applyMiddleware(thunkMiddleware))

const app = (
    <BrowserRouter>
         <Provider store={store}>
            <App/>
         </Provider>
    </BrowserRouter>

)

ReactDOM.render(app,
  document.getElementById('root')
);



