import {CONTACTS_FAILURE, CONTACTS_REQUEST, CONTACTS_SUCCESS} from "./actions";

const initialState = {
    contacts: [],
    error: null,
    loading: false,
}

const reducer=(state= initialState, action)=>{
    switch (action.type){
        case CONTACTS_REQUEST:
            return {...state, error: null, loading: true};
        case CONTACTS_SUCCESS:
            return {...state, loading: false, contacts: action.payload};
        case CONTACTS_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
}

export default reducer;