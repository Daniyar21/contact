import axios from "axios";
import {BASE_URL} from "../BASE_URL";
export const NEW_CONTACT_REQUEST = 'NEW_CONTACT_REQUEST';
export const NEW_CONTACT_SUCCESS = 'NEW_CONTACT_SUCCESS';
export const NEW_CONTACT_FAILURE = 'NEW_CONTACT_FAILURE';

export const CONTACTS_REQUEST = 'CONTACTS_REQUEST';
export const CONTACTS_SUCCESS = 'CONTACTS_SUCCESS';
export const CONTACTS_FAILURE = 'CONTACTS_FAILURE';

export const newContactRequest = ()=>({type:NEW_CONTACT_REQUEST})
export const newContactSuccess = ()=>({type:NEW_CONTACT_SUCCESS})
export const newContactFailure = error=>({type:NEW_CONTACT_FAILURE, payload:error});

export const contactsRequest = ()=>({type:CONTACTS_REQUEST})
export const contactsSuccess = (response)=>({type:CONTACTS_SUCCESS, payload: response})
export const contactsFailure = error=>({type:CONTACTS_FAILURE, payload:error});

export const newContact = (value) =>{
    return async dispatch =>{
        dispatch(newContactRequest());
        try{
          await axios.post(BASE_URL, value);
            dispatch(newContactSuccess());
        } catch (error){
            dispatch(newContactFailure())
        }
    }
}

export const requestContacts = () =>{
    return async dispatch =>{
        dispatch(contactsRequest());
        try{
            const response = await axios.get(BASE_URL);
            dispatch(contactsSuccess(response.data));
        } catch (error){
            dispatch(contactsFailure(error))
        }
    }
}