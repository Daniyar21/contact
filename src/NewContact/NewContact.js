import React, {useState} from 'react';
import './NewContact.css';
import {Link} from "react-router-dom";
import {newContact} from "../store/actions";
import {useDispatch} from "react-redux";


const NewContact = (props) => {
    const dispatch = useDispatch();

    const [user, setUser] = useState({
        fullName: '',
        phone: '',
        email: '',
        image: '',

    })

    const onInputChange = e => {
        const {name, value} = e.target;
        setUser(prev => ({
            ...prev,
            [name]: value
        }));
    };

    let defaultImage = null;

    if(user.image){
        defaultImage = user.image
    } else{
        defaultImage = 'https://okeygeek.ru/wp-content/uploads/2020/03/no_avatar.png';
    }

    const save = (e)=>{
        e.preventDefault();
        dispatch(newContact(user));
        props.history.push('/');
    }


    return (
        <div className='new'>
            <p> Add new contact</p>
            <form onSubmit={(e)=>save(e)} >
            <input
                type='text'
                name='fullName'
                placeholder='Full name'
                value={user.fullName}
                onChange={onInputChange}
            />
            <input
                type='text'
                name='phone'
                placeholder='Phone number'
                value={user.phone}
                onChange={onInputChange}
            />
            <input
                type='email'
                name='email'
                placeholder='Email'
                value={user.email}
                onChange={onInputChange}
            />
            <input
                type='text'
                name='image'
                placeholder='Image'
                value={user.image}
                onChange={onInputChange}
            />
            <img src={defaultImage} alt="user"/>
            <button type='submit'  >Save</button>
            <Link to='/'>Go back</Link>
            </form>
        </div>
    );
};

export default NewContact;