import './App.css';
import {Switch, Route, Link} from "react-router-dom";
import ContactsPage from "./ContactsPage/ContactsPage";
import NewContact from "./NewContact/NewContact";

const App = () => {

  return (
      <div className="App">
          <header>
              <p>My contacts</p>
              <Link to='/new'>Add new contact</Link>
          </header>

        <Switch>
            <Route path='/' exact component={ContactsPage}/>
            <Route path ='/new' component = {NewContact}/>
        </Switch>
      </div>
  );
};

export default App;
