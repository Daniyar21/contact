import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {requestContacts} from "../store/actions";
import './ContactsPage.css';

const ContactsPage = () => {
    const dispatch = useDispatch();
    const state = useSelector(state => state.contacts);

    useEffect(()=>{
        const fetchData = async ()=>{
            await dispatch(requestContacts());
        }
        fetchData();
    },[dispatch])

    const contacts = Object.keys(state).map(key =>{
        return state[key];
    })

    return (
        <div >
            {contacts.map((obj,i) =>(
                <div className='contacts' key={obj.fullName+i}>
                    <img src={obj.image} alt="something"/>
                    <p>{obj.fullName}</p>
                </div>
            ))}
        </div>
    );
};

export default ContactsPage;